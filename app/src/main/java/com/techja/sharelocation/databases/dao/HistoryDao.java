package com.techja.sharelocation.databases.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.techja.sharelocation.databases.entities.HistoryItem;

import java.util.List;

@Dao
public interface HistoryDao {
    @Query("SELECT *FROM HistoryItem")
    List<HistoryItem> getAll();

    @Insert
    void insertAll(HistoryItem... items);

    @Delete
    void delete(HistoryItem item);
}
