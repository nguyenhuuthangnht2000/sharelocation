package com.techja.sharelocation.databases.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class HistoryItem {
    public static final int TYPE_SEND = 1;
    public static final int TYPE_RECEIVE = 0;

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    public int id;
    @ColumnInfo(name = "type")
    public int type;
    @ColumnInfo(name = "lat")
    public double lat;
    @ColumnInfo(name = "lgn")
    public double lgn;
    @ColumnInfo(name = "address")
    public String address;
    @ColumnInfo(name = "time")
    public String time;
    @ColumnInfo(name = "phone")
    public String phone;

    public HistoryItem() {
    }

    public HistoryItem(int type, String address, String time, double lat, double lgn, String phone) {
        this.type = type;
        this.lat = lat;
        this.lgn = lgn;
        this.address = address;
        this.time = time;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return type + "_:_" + address + "_:_" + time + "_:_" + lat + "_:_" + lgn + "_:_" + phone;
    }
}
