package com.techja.sharelocation.databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.techja.sharelocation.databases.dao.HistoryDao;
import com.techja.sharelocation.databases.entities.HistoryItem;

@Database(entities = {HistoryItem.class}, version = 1)
public abstract class AppDB extends RoomDatabase {
    public abstract HistoryDao getDAO();
}
