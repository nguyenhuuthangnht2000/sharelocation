package com.techja.sharelocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapManager {
    public static final String KEY_SHARE_LOCATION = "KEY_SHARE_LOCATION";
    private static final String TAG = MapManager.class.getName();
    private static MapManager instance;
    private GoogleMap mMap;
    private Location myLocation;
    private Marker myMarker;
    private OnActionCallBack callBack;

    public Location getMyLocation() {
        return myLocation;
    }

    public static MapManager getInstance() {
        if (instance == null) {
            instance = new MapManager();
        }
        return instance;
    }

    public void setCallBack(OnActionCallBack callBack) {
        this.callBack = callBack;
    }

    @SuppressLint("VisibleForTests")
    public void initMap(GoogleMap map) {
        Log.i(TAG, "initMap....");
        mMap = map;
        mMap.getUiSettings().setAllGesturesEnabled(true);
        //mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(App.getInstance(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(App.getInstance(), "Bạn chưa cấp quyền vị trí", Toast.LENGTH_SHORT).show();
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMarkerClickListener(marker -> {
            shareLocation(marker);
            return false;
        });

        LocationRequest request = new LocationRequest();
        request.setInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        request.setSmallestDisplacement(100);
        request.setWaitForAccurateLocation(true);

        FusedLocationProviderClient client = new FusedLocationProviderClient(App.getInstance());
        client.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                updateMyLocation(locationResult);
            }
        }, Looper.getMainLooper());
    }

    private void shareLocation(Marker marker) {
        String address = CommonUltis.getInstance().getLocationNameByLatLong(marker.getPosition().latitude, marker.getPosition().longitude);
        myMarker.setSnippet(address);
        callBack.callBack(myMarker, KEY_SHARE_LOCATION);
    }

    private void updateMyLocation(LocationResult rs) {
        Location location = rs.getLocations().get(0);
        Log.i(TAG, "updateMyLocation..." + location.toString());
        if (myLocation == null) {
            myLocation = location;
            LatLng pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
            MarkerOptions op = new MarkerOptions();
            op.title("My Location");
            op.position(pos);
            op.icon(BitmapDescriptorFactory.defaultMarker());
            myMarker = mMap.addMarker(op);
        } else {
            myLocation = location;
        }
    }

    public void updateMyLocation() {
        Log.i(TAG, "updateMyLocation...");
        if (mMap == null || myLocation == null) return;
        LatLng pos = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        myMarker.setPosition(pos);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(pos, 16));
    }
}
