package com.techja.sharelocation;

import android.location.Address;
import android.location.Geocoder;

import java.util.List;
import java.util.Locale;

public class CommonUltis {
    private static CommonUltis instance;

    public static CommonUltis getInstance() {
        if (instance == null) {
            instance = new CommonUltis();
        }
        return instance;
    }

    public String getLocationNameByLatLong(double lat, double lng) {

        String add = "";
        Geocoder geocoder = new Geocoder(App.getInstance(), Locale.getDefault());
        try {
            List<Address> address = geocoder.getFromLocation(lat, lng, 1);
            if (address.size() == 0) {
                return null;
            }
            Address obj = address.get(0);
            add = obj.getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return add;
    }
}
