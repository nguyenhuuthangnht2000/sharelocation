package com.techja.sharelocation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.techja.sharelocation.databases.entities.HistoryItem;

import java.util.List;

public class SMSRecevie extends BroadcastReceiver {
    private static final CharSequence SYMBOL_SMS = "CHIA_SE_VI_TRI";
    private static final String SPACE = "_:_";
    private static final String TAG = SMSRecevie.class.getName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "OnReceive...");
        Bundle bundle = intent.getExtras();
        StringBuilder sms = new StringBuilder();
        Object[] pdu = (Object[]) bundle.get("pdus");
        for (Object o : pdu) {
            SmsMessage msg = SmsMessage.createFromPdu((byte[]) o);
            sms.append(msg.getMessageBody());
        }
        String text = sms.toString();
        if (text.contains(SYMBOL_SMS)) {
            Log.i(TAG, "OnReceive..." + text);
            text = text.replace(SYMBOL_SMS, "");
            String[] parts = text.split(SPACE);
            if (parts.length < 5) return;
            String type = parts[0];
            String address = parts[1];
            String time = parts[2];
            String lat = parts[3];
            String lgn = parts[4];
            String phone = parts[5];
            HistoryItem item = new HistoryItem(HistoryItem.TYPE_RECEIVE, address, time,
                    Double.parseDouble(lat), Double.parseDouble(lgn), phone);
            //add to storage
            List<HistoryItem> listItem = App.getInstance().getStorage().historyItems.getValue();
            if (listItem != null) {
                listItem.add(item);
            }
            App.getInstance().getStorage().historyItems.setValue(listItem);
            new Thread() {
                @Override
                public void run() {
                    App.getInstance().getDb().getDAO().insertAll(item);
                    Log.i(TAG, "Data is saved");
                }
            }.start();
        }
    }
}
