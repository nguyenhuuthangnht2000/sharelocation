package com.techja.sharelocation.dialog;

import android.Manifest;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.techja.sharelocation.App;
import com.techja.sharelocation.OnActionCallBack;
import com.techja.sharelocation.R;
import com.techja.sharelocation.databases.dao.HistoryDao;
import com.techja.sharelocation.databases.entities.HistoryItem;

import java.util.ArrayList;
import java.util.Objects;

public class ShareLocationDialog extends Dialog {
    private static final String KEY_REQUEST_SMS_PERMISSION = "KEY_REQUEST_SMS_PERMISSION";
    private static final String TAG = ShareLocationDialog.class.getName();
    private final HistoryItem data;
    private EditText edtPhone;
    private final OnActionCallBack callBack;

    public ShareLocationDialog(@NonNull Context context, HistoryItem data, OnActionCallBack callBack) {
        super(context);
        setContentView(R.layout.view_share_location);
        this.data = data;
        this.callBack = callBack;
        initViews();
    }

    private void initViews() {
        TextView tvAddress = findViewById(R.id.tv_address);
        TextView tvTime = findViewById(R.id.tv_time);
        edtPhone = findViewById(R.id.edt_phone);
        tvAddress.setText(String.format("Bạn đang ở {%s}", data.address));
        tvTime.setText(data.time);
        findViewById(R.id.bt_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLocation();
            }
        });
    }

    private void shareLocation() {
        if (edtPhone.getText().toString().isEmpty()) {
            Toast.makeText(App.getInstance(), "Hãy nhập số điện thoại trước", Toast.LENGTH_SHORT).show();
            return;
        }
        data.phone = (edtPhone.getText().toString());
        Objects.requireNonNull(App.getInstance().getStorage().historyItems.getValue()).add(data);

        if (App.getInstance().checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            callBack.callBack(null, KEY_REQUEST_SMS_PERMISSION);
            return;
        }

        String messageToSend = "CHIA_SE_VI_TRI" + data.toString();
        String number = data.phone;
        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(messageToSend);

        ArrayList<PendingIntent> sendList = new ArrayList<>();
        ArrayList<PendingIntent> deliverList = new ArrayList<>();

        sms.sendMultipartTextMessage(number, null, parts, sendList, deliverList);

        dismiss();

        saveToDB();
    }

    private void saveToDB() {
        new Thread() {
            @Override
            public void run() {
                HistoryDao dao = App.getInstance().getDb().getDAO();
                dao.insertAll(data);
                Log.i(TAG, "Data is saved");
            }
        }.start();
    }
}
