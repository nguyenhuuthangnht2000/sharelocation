package com.techja.sharelocation.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.sharelocation.App;
import com.techja.sharelocation.MapManager;
import com.techja.sharelocation.OnActionCallBack;
import com.techja.sharelocation.R;
import com.techja.sharelocation.activity.MainActivity;
import com.techja.sharelocation.adapter.HistoryAdapter;
import com.techja.sharelocation.base.BaseFragment;
import com.techja.sharelocation.databases.dao.HistoryDao;
import com.techja.sharelocation.databases.entities.HistoryItem;
import com.techja.sharelocation.databinding.FrgM002HistoryBinding;
import com.techja.sharelocation.viewmodel.M002ViewModel;

import java.util.List;
import java.util.Objects;

public class M002HistoryFrg extends BaseFragment<FrgM002HistoryBinding, M002ViewModel> {
    public static final String TAG = M002HistoryFrg.class.getName();

    @Override
    protected void initViews() {
        mBinding.include.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity mainActivity = (MainActivity) mContext;
                mainActivity.onBackPressed();
            }
        });
        updateRecylerView();
        showListHistory();
        App.getInstance().getStorage().historyItems.observe(this, historyItems -> {
            HistoryAdapter adapter = (HistoryAdapter) mBinding.rvData.getAdapter();
            assert adapter != null;
            adapter.setData(historyItems);
        });
    }

    private void showListHistory() {
        new Thread() {
            @Override
            public void run() {
                if (App.getInstance().getStorage().historyItems.getValue() != null) {

                    List<HistoryItem> listData = App.getInstance().getDb().getDAO().getAll();
                    MainActivity mainActivity = (MainActivity) mContext;
                    mainActivity.runOnUI(new OnActionCallBack() {
                        @Override
                        public void callBack(Object data, String key) {
                            App.getInstance().getStorage().historyItems.setValue(listData);
                        }
                    });
                }
            }
        }.start();
    }

    private void updateRecylerView() {
        if (App.getInstance().getStorage().historyItems.getValue() != null) {
            Log.i(TAG, "ListData" + App.getInstance().getStorage().historyItems.getValue().size());
            HistoryAdapter adapter = new HistoryAdapter(App.getInstance().getStorage().historyItems.getValue(), mContext, new OnActionCallBack() {
                @Override
                public void callBack(Object data, String key) {
                    showMap((HistoryItem) data);
                }
            });
            mBinding.rvData.setAdapter(adapter);
            attachSwipeToRV(adapter);
        }
    }

    private void attachSwipeToRV(HistoryAdapter adapter) {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                HistoryItem delItem = Objects.requireNonNull(App.getInstance().getStorage().historyItems.getValue()).get(position);

                AlertDialog dialog = new AlertDialog.Builder(mContext).create();
                dialog.setTitle("Thông báo");
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setMessage("Bạn có muốn xóa dữ liệu: \n" + delItem.address + "?");

                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        new Thread() {
                            @Override
                            public void run() {
                                HistoryDao dao = App.getInstance().getDb().getDAO();
                                dao.delete(delItem);
                            }
                        }.start();

                        App.getInstance().getStorage().historyItems.getValue().remove(position);
                        adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                    }
                });
                dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", (dialogInterface, i) -> {
                    adapter.notifyItemChanged(position);
                });
                dialog.show();
            }
        }).attachToRecyclerView(mBinding.rvData);
    }

    private void showMap(HistoryItem data) {
        String path = "http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s";
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse(String.format(path, MapManager.getInstance().getMyLocation().getLatitude(),
                        MapManager.getInstance().getMyLocation().getLongitude(), data.lat, data.lgn)));
        startActivity(intent);
    }

    @Override
    protected void clickView(View view) {

    }

    @Override
    protected Class<M002ViewModel> initViewModel() {
        return M002ViewModel.class;
    }

    @Override
    protected FrgM002HistoryBinding initViewBinding(View view) {
        return FrgM002HistoryBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m002_history;
    }
}
