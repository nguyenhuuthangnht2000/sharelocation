package com.techja.sharelocation.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Marker;
import com.techja.sharelocation.App;
import com.techja.sharelocation.MapManager;
import com.techja.sharelocation.OnActionCallBack;
import com.techja.sharelocation.R;
import com.techja.sharelocation.activity.MainActivity;
import com.techja.sharelocation.base.BaseFragment;
import com.techja.sharelocation.databases.entities.HistoryItem;
import com.techja.sharelocation.databinding.FrgM001MainBinding;
import com.techja.sharelocation.dialog.ShareLocationDialog;
import com.techja.sharelocation.viewmodel.M001ViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;

public class M001MainFrg extends BaseFragment<FrgM001MainBinding, M001ViewModel> {
    public static final String TAG = M001MainFrg.class.getName();

    @Override
    protected void initViews() {
        MainActivity mainActivity = (MainActivity) mContext;
        mainActivity.checkLocationPermission();
        SupportMapFragment mapFrg = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_frg);
        assert mapFrg != null;
        mapFrg.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull GoogleMap map) {
                MapManager.getInstance().initMap(map);
            }
        });
        MapManager.getInstance().setCallBack(new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                if (key.equals(MapManager.KEY_SHARE_LOCATION)) {
                    shareLocation(data);
                }
            }
        });
        mBinding.ivMyLocation.setOnClickListener(this);
        mBinding.include.ivHistory.setOnClickListener(this);

        if (App.getInstance().checkSelfPermission(Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED||
                App.getInstance().checkSelfPermission(Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.RECEIVE_SMS}, 101);
        }
    }

    private void shareLocation(Object data) {
        Marker marker = (Marker) data;
        HistoryItem item = new HistoryItem(HistoryItem.TYPE_SEND, marker.getSnippet(), getTimeNow(),
                marker.getPosition().latitude, marker.getPosition().longitude, null);
        ShareLocationDialog dialog = new ShareLocationDialog(mContext, item, new OnActionCallBack() {
            @Override
            public void callBack(Object data, String key) {
                requestPermissions(new String[]{
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.RECEIVE_SMS,
                }, 101);
            }
        });
        dialog.show();
    }

    private String getTimeNow() {
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
        return "Vào hồi: " + df.format(new Date());
    }

    @Override
    protected void clickView(View view) {
        if (view.getId() == R.id.iv_my_location) {
            MapManager.getInstance().updateMyLocation();
        } else if (view.getId() == R.id.iv_history) {
            showHistory();
        }
    }

    private void showHistory() {
        MainActivity mainActivity = (MainActivity) mContext;
        mainActivity.showFrg(M002HistoryFrg.TAG, true);
    }


    @Override
    protected Class<M001ViewModel> initViewModel() {
        return M001ViewModel.class;
    }

    @Override
    protected FrgM001MainBinding initViewBinding(View view) {
        return FrgM001MainBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.frg_m001_main;
    }
}
