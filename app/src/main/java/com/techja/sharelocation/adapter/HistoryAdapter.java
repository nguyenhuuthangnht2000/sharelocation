package com.techja.sharelocation.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.techja.sharelocation.OnActionCallBack;
import com.techja.sharelocation.R;
import com.techja.sharelocation.databases.entities.HistoryItem;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
    private final Context context;
    private List<HistoryItem> listItem;
    private OnActionCallBack callBack;

    public HistoryAdapter(List<HistoryItem> listItem, Context context, OnActionCallBack callBack) {
        this.listItem = listItem;
        this.context = context;
        this.callBack = callBack;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_history, parent, false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
        HistoryItem item = listItem.get(position);
        holder.tvAddress.setText(item.address);
        holder.tvAddress.setTextColor(item.type == HistoryItem.TYPE_SEND ? Color.parseColor("#2196F3") : Color.RED);
        holder.tvTime.setText(String.format("Vào hồi: %s", item.time));
        holder.tvLocation.setText(String.format("Vị trí: %s,%s", item.lat, item.lgn));
        holder.tvPhone.setText(item.phone);
        holder.ivType.setImageLevel(item.type);
        holder.ivType.setColorFilter(ContextCompat.getColor(context, item.type == HistoryItem.TYPE_SEND ? R.color.blue : R.color.red));
        holder.ivAddress.setColorFilter(ContextCompat.getColor(context, item.type == HistoryItem.TYPE_SEND ? R.color.blue : R.color.red));
        holder.tvAddress.setTag(item);
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setData(List<HistoryItem> historyItems) {
        listItem = historyItems;
        notifyDataSetChanged();
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvAddress, tvTime, tvLocation, tvPhone;
        ImageView ivType, ivAddress;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tv_address);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvLocation = itemView.findViewById(R.id.tv_pos);
            ivType = itemView.findViewById(R.id.iv_type);
            tvPhone = itemView.findViewById(R.id.tv_phone);
            ivAddress = itemView.findViewById(R.id.iv_address);
            itemView.findViewById(R.id.iv_gps).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemView.startAnimation(AnimationUtils.loadAnimation(context, androidx.appcompat.R.anim.abc_shrink_fade_out_from_bottom));
                    callBack.callBack(tvAddress.getTag(), null);
                }
            });
        }
    }
}
