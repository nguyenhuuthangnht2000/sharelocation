package com.techja.sharelocation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.techja.sharelocation.databases.entities.HistoryItem;

import java.util.ArrayList;
import java.util.List;

public class Storage {
    public MutableLiveData<List<HistoryItem>> historyItems = new MutableLiveData<>(new ArrayList<>());
}
