package com.techja.sharelocation.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.view.View;

import com.techja.sharelocation.OnActionCallBack;
import com.techja.sharelocation.R;
import com.techja.sharelocation.base.BaseActivity;
import com.techja.sharelocation.databinding.ActivityMainBinding;
import com.techja.sharelocation.fragment.M001MainFrg;
import com.techja.sharelocation.viewmodel.MainViewModel;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> {
    @Override
    protected void initViews() {
        showFrg(M001MainFrg.TAG);
    }

    @Override
    protected ActivityMainBinding initViewBinding(View view) {
        return ActivityMainBinding.bind(view);
    }

    @Override
    protected int getLayOutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class<MainViewModel> initViewModel() {
        return MainViewModel.class;
    }

    public void runOnUI(OnActionCallBack callBack) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callBack.callBack(null, null);
            }
        });
    }

    public void checkLocationPermission() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
            }, 101);
        }
    }
}