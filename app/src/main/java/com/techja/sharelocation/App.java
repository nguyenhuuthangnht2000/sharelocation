package com.techja.sharelocation;

import android.app.Application;

import androidx.room.Room;

import com.techja.sharelocation.databases.AppDB;

public class App extends Application {
    private static App instance;
    private Storage storage;
    private AppDB db;

    public static App getInstance() {
        return instance;
    }

    public Storage getStorage() {
        return storage;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        storage = new Storage();
        configDB();
    }

    public AppDB getDb() {
        return db;
    }

    private void configDB() {
        db = Room.databaseBuilder(this, AppDB.class, "location-db").build();
    }
}
